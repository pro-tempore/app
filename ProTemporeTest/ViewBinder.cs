﻿using Android.Views;
using Android.Widget;
using System;

namespace ProTemporeTest
{
    class ViewBinder : Java.Lang.Object, SimpleCursorAdapter.IViewBinder
    {
        public bool SetViewValue(View view, Android.Database.ICursor cursor, int columnIndex)
        {
            if (columnIndex == 2)
            {
                long ms = cursor.GetLong(columnIndex);

                DateTime date = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc).AddMilliseconds(ms).ToLocalTime();

                TextView textView = (TextView)view;
                textView.Text = date.ToLongDateString();

                return true;
            }
            return false;
        }
    }
}
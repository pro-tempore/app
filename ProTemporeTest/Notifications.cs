using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Database;

namespace ProTemporeTest
{
    [Activity(Label = "Notifications")]
    public class Notifications : ListActivity
    {

        Menu menu;

        NotMsg notmsg;
        String serverAddr = "http://130.240.5.35:8000/";
        SimpleCursorAdapter adapter;
        string[] _calendarsProjection, _sourceColumns;
        ICursor cursor;
        string token;

        private class Notification
        {

        }

        private class NotMsg
        {
            public String Type { get; set; }
            public String token { get; set; }
        }

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            SetContentView(Resource.Layout.Notifications);
            menu = MainActivity.getMenu();
            menu.setActivity(this, this.BaseContext);
            FindViewById<TextView>(Resource.Id.txtActionBarText).Text = "Notifications";
            token = MainActivity.getToken();
            GetNotifications(token);

        }

        void GetNotifications(String username) {
            notmsg = new NotMsg() { Type = "Notifications" ,token = username };

            // Make a HTTP call to the server to get a list of all notifications for this user.


            // Temporary list
            String[] list = { "Work", "Work2", "Work3", "Work4", "Work5" };


            //Simple listview to display the Notifications
            

            ListAdapter = new ArrayAdapter<String>(this, Android.Resource.Layout.SimpleListItem1, list);


        }
    }
}
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

using Android.Views.Animations;
using Android.Graphics;

using Android.Util;

namespace ProTemporeTest
{
    public class Menu
    {
        GestureDetector gestureDetector;
        GestureListener gestureListener;

        Context context;
        Activity activity;

        Intent[] activities;

        ListView menuListView;
        MenuListAdapterClass objAdapterMenu;
        ImageView menuIconImageView;
        int intDisplayWidth;
        bool isSingleTapFired = false;
        TextView txtActionBarText;
        TextView txtPageName;
        TextView txtDescription;
        ImageView btnDescExpander;

        int CalendarID;

        public Menu(Activity a, Context c) {
            activity = a;
            context = c;
            
        }

        public void setActivity(Activity a, Context c) {
            activity = a;
            context = c;
            FnInitialization();
            TapEvent();
            FnBindMenu();
        }

        public int GetCalendar() {
            Log.Info("ProTempore", "trying to get CalendarID: ", CalendarID);
            return CalendarID;
        }

        public void setCalendarId(int id) {
            CalendarID = id;
        }

        void TapEvent()
        {
            //title bar menu icon
            menuIconImageView.Click += delegate (object sender, EventArgs e)
            {
                if (!isSingleTapFired)
                {
                    FnToggleMenu();  //find definition in below steps
                    isSingleTapFired = false;
                }
            };
            //bottom expandable description window
            /*btnDescExpander.Click += delegate (object sender, EventArgs e)
            {
                //FnDescriptionWindowToggle();
            };*/
        }
        void FnInitialization()
        {
            //gesture initialization
            gestureListener = new GestureListener();
            gestureListener.LeftEvent += GestureLeft; //find definition in below steps
            gestureListener.RightEvent += GestureRight;
            gestureListener.SingleTapEvent += SingleTap;
            gestureDetector = new GestureDetector(activity, gestureListener);

            menuListView = activity.FindViewById<ListView>(Resource.Id.menuListView);
            menuIconImageView = activity.FindViewById<ImageView>(Resource.Id.menuIconImgView);
            txtActionBarText = activity.FindViewById<TextView>(Resource.Id.txtActionBarText);
            //txtPageName = FindViewById<TextView>(Resource.Id.txtPage);
            //txtDescription = FindViewById<TextView>(Resource.Id.txtDescription);
            //btnDescExpander = FindViewById<ImageView>(Resource.Id.btnImgExpander);

            //changed sliding menu width to 3/4 of screen width 
            Display display = activity.WindowManager.DefaultDisplay;
            var point = new Point();
            display.GetSize(point);
            intDisplayWidth = point.X;
            intDisplayWidth = intDisplayWidth - (intDisplayWidth / 3);
            using (var layoutParams = menuListView.LayoutParameters)
            {
                layoutParams.Width = intDisplayWidth;
                layoutParams.Height = point.Y;
                //layoutParams.Height = ViewGroup.LayoutParams.MatchParent;
                menuListView.LayoutParameters = layoutParams;
            }
        }

        void FnBindMenu()
        {
            string[] strMnuText = { "Check in" , "Calender"};
            activities = new Intent[3];
            int[] strMnuUrl = { Resource.Drawable.check_in_icon, Resource.Drawable.Calendar_icon };
            if (objAdapterMenu != null)
            {
                objAdapterMenu.actionMenuSelected -= FnMenuSelected;
                objAdapterMenu = null;
            }
            objAdapterMenu = new MenuListAdapterClass(activity, strMnuText, strMnuUrl);
            objAdapterMenu.actionMenuSelected += FnMenuSelected;
            menuListView.Adapter = objAdapterMenu;
        }


        void FnMenuSelected(string strMenuText)
        {
            if (strMenuText == null)
            {
                return;
            }

            txtActionBarText.Text = strMenuText;
            //txtPageName.Text = strMenuText;

            if (strMenuText == "Check in")
            {
                Intent intent = new Intent(context.ApplicationContext, typeof(GeoLoc));
 
                Log.Info("ProTempore", "Check in");
                intent.SetFlags(ActivityFlags.NewTask);
                context.StartActivity(intent);

                
            }
            if (strMenuText == "Calender")
            {
                Intent intent = new Intent(context.ApplicationContext, typeof(Calendar));

                Log.Info("ProTempore", "Calendar");
                intent.SetFlags(ActivityFlags.NewTask);
                context.StartActivity(intent);
                
            }
            /*if (strMenuText == "Notifications")
            {
                Intent intent = new Intent(context.ApplicationContext, typeof(Notifications));

                Log.Info("ProTempore", "notifications");
                intent.SetFlags(ActivityFlags.NewTask);
                context.StartActivity(intent);

                
            }*/

        }

        void GestureLeft()
        {
            if (!menuListView.IsShown)
                FnToggleMenu();
            isSingleTapFired = false;
        }
        void GestureRight()
        {
            if (menuListView.IsShown)
                FnToggleMenu();
            isSingleTapFired = false;
        }
        void SingleTap()
        {
            if (menuListView.IsShown)
            {
                FnToggleMenu();
                isSingleTapFired = true;
            }
            else
            {
                isSingleTapFired = false;
            }
        }

        void FnToggleMenu()
        {
            Console.WriteLine(menuListView.IsShown);
            if (menuListView.IsShown)
            {
                menuListView.Animation = new TranslateAnimation(0f, -menuListView.MeasuredWidth, 0f, 0f);
                menuListView.Animation.Duration = 300;
                menuListView.Visibility = ViewStates.Gone;
            }
            else
            {
                menuListView.Visibility = ViewStates.Visible;
                menuListView.RequestFocus();
                menuListView.Animation = new TranslateAnimation(-menuListView.MeasuredWidth, 0f, 0f, 0f);//starting edge of layout 
                menuListView.Animation.Duration = 300;
            }
        }

        public bool DispatchTouchEvent(MotionEvent ev)
        {
            gestureDetector.OnTouchEvent(ev);
            return activity.DispatchTouchEvent(ev);
        }
    }

   
}
﻿//EventListActivity specifies the contents of a calendar.

using Android.App;
using Android.Database;
using Android.OS;
using Android.Provider;
using Android.Net;
using System;
using Android.Widget;
using Android.Util;

namespace ProTemporeTest
{
    [Activity(Label = "EventListActivity")]
    internal class EventListActivity : ListActivity
    {
        int _calId;
        ICursor cursor;
        Android.Net.Uri eventsUri;


        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            //Get the calendar id from the intent used to create this activity.
            _calId = Intent.GetIntExtra("calId", -1);

            //Get the Uri for requesting a list of events.
            eventsUri = CalendarContract.Events.ContentUri;

            //Specify what we are interested from the event.
            //Id used to iterate over events.
            //Title is simply the name of the event.
            //Dtstart is the start date of the event.
            string[] eventsProjection = {
                CalendarContract.Events.InterfaceConsts.Id,
                CalendarContract.Events.InterfaceConsts.Title,
                CalendarContract.Events.InterfaceConsts.Dtstart
            };

            //Create a pointer for calendar events.
            cursor = ManagedQuery(eventsUri, eventsProjection, string.Format("calendar_id={0}", _calId), null, "dtstart ASC");

            //Get the titles and startdates.
            string[] sourceColumns = {
                CalendarContract.Events.InterfaceConsts.Title,
                CalendarContract.Events.InterfaceConsts.Dtstart
            };

            //Project them on the following interface objects.
            int[] targetResources = {
                Resource.Id.eventTitle,
                Resource.Id.eventStartDate
            };

            //Create an adapter that can actually do the projecting from data to ui elements.
            var adapter = new SimpleCursorAdapter(this, Resource.Layout.EventListItem, cursor, sourceColumns, targetResources);

            //Create a viewbinder object which allows us more fine grain control over for example the formatting.
            adapter.ViewBinder = new ViewBinder();

            ListAdapter = adapter;

        }
    }
}
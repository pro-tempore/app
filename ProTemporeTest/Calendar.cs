//Main activity for Calendar viewing.
//Gets all calendars on the phone and can display the content of them as a list.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Provider;
using Android.Database;
using Android.Net;
using Android.Util;


namespace ProTemporeTest
{
    [Activity(Label = "Calendar")]
    public class Calendar : ListActivity
    {
        Android.Net.Uri _calendarsUri;
        string[] _calendarsProjection, _sourceColumns;
        int[] _targetResources;
        ICursor cursor;
        SimpleCursorAdapter adapter;
        Button backbutton;

        bool existed;
        int calID;

        Menu menu;

        string[] newCheckIns;

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            SetContentView(Resource.Layout.Calendar);

            loadCals();

            menu = MainActivity.getMenu();
            menu.setActivity(this, this.BaseContext);
            FindViewById<TextView>(Resource.Id.txtActionBarText).Text = "Calendar";
            

            // Check to see if we have an App Calendar.
            initNewCal();
            // If we created a new calendar, reload the list with calendars
            if (!existed) {
                loadCals();
            }

            backbutton = FindViewById<Button>(Resource.Id.Back_button_calendar);

            OpenCal();

            backbutton.Click += delegate {
                ButtonPressed();
            };

            //Start capturing click events on the calendar list.
            /*ListView.ItemClick += (sender, e) => {
                calendarListClicked(sender, e);   
            };*/

        }

        private void ButtonPressed() {
            Intent intent = new Intent(this.ApplicationContext, typeof(GeoLoc));

            Log.Info("ProTempore", "Check in");
            intent.SetFlags(ActivityFlags.NewTask);
            this.StartActivity(intent);
        }

        private void OpenCal() {

            //Set the pointer to that object.
            cursor.MoveToPosition(calID - 1);

            //Actually get the id int.
            int calId = cursor.GetInt(cursor.GetColumnIndex(_calendarsProjection[0]));

            //Create an intent to open the phones calendar application.
            var calIntent = new Intent(Intent.ActionView, Android.Net.Uri.Parse("content://com.android.calendar/time/"));

            try
            {
                Log.Info("ProTempore", "Showing Calendar {0}", calId);
                StartActivity(calIntent);
            }
            catch (Exception err)
            {
                Log.Info("ProTempore", "Failed to show event: {0}: ", err.ToString());
            }
        }

        private void loadCals() {
            _calendarsUri = CalendarContract.Calendars.ContentUri;


            //Specifies what we are interested in for calendars.
            //Id is used for handling click events on the calendar list.
            //CalendarDisplayName is the actual name of the calendar.
            //AccountName shows which account the calendar is attached to.
            _calendarsProjection = new string[]{
                CalendarContract.Calendars.InterfaceConsts.Id,
                CalendarContract.Calendars.InterfaceConsts.CalendarDisplayName,
                CalendarContract.Calendars.InterfaceConsts.AccountName
            };


            //ManagedQuery is Deprecated but I have not found a way to get the replacement to work.
            //From what I understand of cursors, they seem pretty analogue to pointers.
            cursor = ManagedQuery(_calendarsUri, _calendarsProjection, null, null, null);


            //Get the available calendars.
            _sourceColumns = new string[]
            {
                CalendarContract.Calendars.InterfaceConsts.CalendarDisplayName,
                CalendarContract.Calendars.InterfaceConsts.AccountName
            };

            //Specify what interface items we should project the calendar list on.
            _targetResources = new int[]{
                Resource.Id.calDisplayName,
                Resource.Id.calAccountName
            };

            //Write the list of calendars to the layout.
            //adapter = new SimpleCursorAdapter(this, Resource.Layout.ListItem, cursor, _sourceColumns, _targetResources);
            //ListAdapter = adapter;
        }

        private void initNewCal() {
            existed = false;
            var prefs = Application.Context.GetSharedPreferences("ProTempore", FileCreationMode.Private);
            var prefEditor = prefs.Edit();
            for (int i = 0; i < cursor.Count; i++) {
                cursor.MoveToPosition(i);
                String calName = cursor.GetString(cursor.GetColumnIndex(_calendarsProjection[1]));
                
                if (calName == "Pro Tempore") {
                    existed = true;
                    calID = cursor.GetInt(cursor.GetColumnIndex(_calendarsProjection[0]));
                    Log.Info("ProTempore", "Setting ID for protempore calendar: {0}", calID);
                    menu.setCalendarId(calID);

                    
                    prefEditor.PutString("cal", calID.ToString());
                    prefEditor.Commit();

                    return;
                }
            }

            var uri = CalendarContract.Calendars.ContentUri;
            ContentValues val = new ContentValues();
            val.Put(CalendarContract.Calendars.InterfaceConsts.AccountName, "account_name");
            val.Put(CalendarContract.Calendars.InterfaceConsts.AccountType, CalendarContract.AccountTypeLocal);
            val.Put(CalendarContract.Calendars.Name, "My Check ins");
            val.Put(CalendarContract.Calendars.InterfaceConsts.CalendarDisplayName, "Pro Tempore");
            val.Put(CalendarContract.Calendars.InterfaceConsts.CalendarColor, Android.Graphics.Color.Red);
            val.Put(CalendarContract.Calendars.InterfaceConsts.OwnerAccount, "ownerAccount");
            val.Put(CalendarContract.Calendars.InterfaceConsts.CalendarAccessLevel, CalendarAccess.AccessOwner.ToString());
            val.Put(CalendarContract.Calendars.InterfaceConsts.Visible, true);
            val.Put(CalendarContract.Calendars.InterfaceConsts.SyncEvents, true);
            uri = uri.BuildUpon()
                .AppendQueryParameter(CalendarContract.CallerIsSyncadapter, "true")
                .AppendQueryParameter(CalendarContract.Calendars.InterfaceConsts.AccountName, "account_name")
                .AppendQueryParameter(CalendarContract.Calendars.InterfaceConsts.AccountType, CalendarContract.AccountTypeLocal)
                .Build();
            var calresult = ContentResolver.Insert(uri, val);
            calID = int.Parse(calresult.LastPathSegment);
            Log.Info("ProTempore", "ID for protempore calendar: {0}", calID);
            menu.setCalendarId(calID);

            prefEditor.PutString("cal", calID.ToString());
            prefEditor.Commit();


        }

        private void calendarListClicked(object sender, AdapterView.ItemClickEventArgs e)
        {
            //Which id was clicked?
            int i = (e as AdapterView.ItemClickEventArgs).Position;

            //Set the pointer to that object.
            cursor.MoveToPosition(i);
          
            //Actually get the id int.
            int calId = cursor.GetInt(cursor.GetColumnIndex(_calendarsProjection[0]));

            //Create an intent to open the phones calendar application.
            var calIntent = new Intent(Intent.ActionView, Android.Net.Uri.Parse("content://com.android.calendar/time/"));

            //XXX Future work Find a way to make only the selected calendar visible. This is easy when doing our own viewer.
            //but using the phones built in one I have not found a safe way to do this.

            //Try to start the calendar activity.
            try
            {
                Log.Info("ProTempore", "Showing Calendar {0}", calId);
                StartActivity(calIntent);
            }
            catch (Exception err)
            {
                Log.Info("ProTempore", "Failed to show event: {0}: ", err.ToString());
            }
            
      
        }

    }
}
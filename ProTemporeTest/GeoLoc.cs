using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SharedDepository;
using System.Collections.Specialized;
using System.Net;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Locations;
using Android.Util;
using Java.Net;
using Java.IO;
using System.Net.Sockets;
using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Net.Http;
using Android.Provider;
using Java.Util;

namespace ProTemporeTest
{
    [Activity(Label = "GeoLoc")]
    public class GeoLoc : Activity, ILocationListener
    {
        
        TextView _locationText;
        TextView _addressText;
        TextView _dbResponse;
        string _currentLongitude;
        string _currentLatitude;
        string serverAddr;
        string token;
        string username;

        Location _currentLocation;
        Location _liveLocation;
        LocationManager _locationManager;
        IList<string> providers;
        string _locationProvider;

        Menu menu;
        int calID;

        CheckBox checkbox1;
        CheckBox checkbox2;

        string check = "";

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            SetContentView(Resource.Layout.GeoLoc);

            _addressText = FindViewById<TextView>(Resource.Id.address_text);
            _locationText = FindViewById<TextView>(Resource.Id.location_text);
            _dbResponse = FindViewById<TextView>(Resource.Id.dbResponse_text);
            FindViewById<TextView>(Resource.Id.get_data_button).Click += DataButton_OnClick;
            FindViewById<TextView>(Resource.Id.get_stampla_button).Click += StamplaButton_OnClick;

            serverAddr = MainActivity.getAddress();
            token = MainActivity.getToken();
            username = MainActivity.getUsername();
            menu = MainActivity.getMenu();
            menu.setActivity(this, this.BaseContext);
            Log.Info("ProTempore", "Got menu from main");
            FindViewById<TextView>(Resource.Id.txtActionBarText).Text = "Check In";

            initLocationManager();

            checkbox1 = FindViewById<CheckBox>(Resource.Id.checkbox1);
            checkbox2 = FindViewById<CheckBox>(Resource.Id.checkbox2);

            checkbox1.Click += (o, e) => {
                if (checkbox1.Checked)
                {
                    checkbox2.Checked = false;
                    check = "Check in";
                }
                else
                {
                    checkbox2.Checked = true;
                    check = "Check out";
                }
            };
            checkbox2.Click += (o, e) => {
                if (checkbox2.Checked)
                {
                    checkbox1.Checked = false;
                    check = "Check out";
                }
                else
                {
                    checkbox1.Checked = true;
                    check = "Check in";
                }
            };

        }

        protected void initLocationManager()
        {
            //Get a manager.
            _locationManager = (LocationManager)GetSystemService(LocationService);

            //Set the accuracy to fine grained.
            Criteria crit = new Criteria { Accuracy = Accuracy.Fine };

            //Get a list of location providers and a list of fine providers if any.
            providers = _locationManager.GetProviders(true);
            IList<string> fineProviders = _locationManager.GetProviders(crit, true);
            if (fineProviders.Any())
            {
                _locationProvider = fineProviders.First();
                Log.Info("ProTempore", "Fine provider found.");
            }
            else if (providers.Any())
            {
                _locationProvider = providers.First();
                Log.Info("ProTempore", "No fine provider, using coarse.");
            }
            else
            {
                _locationProvider = String.Empty;
                Log.Error("ProTempore", "initLocationManager: Failed to set Locationprovider.");
            }
        }

        protected override void OnResume()
        {
            base.OnResume();
            //menu.setActivity(this, this.BaseContext);
            _locationManager.RequestLocationUpdates(_locationProvider, 0, 0, this);
            Log.Info("ProTempore", "GEOLOC: OnResume()");
        }

        protected override void OnPause()
        {
            base.OnPause();
            _locationManager.RemoveUpdates(this);
            Log.Info("ProTempore", "GEOLOC: OnPause()");
        }

        async void DataButton_OnClick(object sender, EventArgs eventArgs)
        {
            _currentLocation = getLocation();
            if (_currentLocation == null)
            {
                Log.Info("ProTempore", "current location = null");
                _addressText.Text = "Can't determine the current position. Please try moving the phone around and retry.";
                return;
            }
            _currentLongitude = _currentLocation.Longitude.ToString();
            _currentLatitude = _currentLocation.Latitude.ToString();
            _addressText.Text = "Location: " + _currentLatitude + ", " + _currentLongitude;

        }

        

        private async void StamplaButton_OnClick(object sender, EventArgs e)
        {
            Log.Info("ProTempore", "starting a transmit to server: {0}", check);
            SendStamp stampSender = new SendStamp(serverAddr);

            CreateCheckInEvent(_currentLatitude, _currentLongitude);

            Log.Info("ProTempore", "have tried to create an event in Calendar: {0}", calID);

            if ((_currentLongitude ?? _currentLatitude) == null)

            {
                Log.Info("ProTempore", "Current location unavailable. Can't send time stamp.");
                _dbResponse.Text = "Failed to send timestamp, location not available yet.";
            }
            else
            {
                Log.Info("ProTempore", "Sending stuff to db.");
                _dbResponse.Text = "Transmitting to server.";

                Exception err = await stampSender.transmit(_currentLongitude, _currentLatitude, token, username, check);

                if(err == null)
                {
                    Log.Info("ProTempore", "Stamp successfully transmitted to server.");
                    _dbResponse.Text = "Transmission succeeded";

                    if (check == "Check in")
                    {
                        checkbox1.Checked = false;
                        checkbox2.Checked = true;
                        check = "Check out";
                    }
                    else {
                        checkbox2.Checked = false;
                        checkbox1.Checked = true;
                        check = "Check in";
                    }
                } else
                {
                    Log.Error("ProTempore", "Failed to send stamp, ERROR: {0}", err);
                    _dbResponse.Text = "Failed to send stamp.";
                }
            }
        }

        private void CreateCheckInEvent(string lat, string lon)
        {
            // Cretes an event in the calendar for this check in. In case of bad net access
            // Check ins are stored locally on the phone for later views.

            var prefs = Application.Context.GetSharedPreferences("ProTempore", FileCreationMode.Private);
            string cal = prefs.GetString("cal", null);

            calID = Int32.Parse(cal);

            Log.Info("ProTempore", "trying to create an event in Calendar: {0}", calID);
            //calID = menu.GetCalendar();
            //calID = 13;
            if (calID == null) {
                Log.Info("ProTempore","calID == null");
                return;
            }

            long timeL = Java.Lang.JavaSystem.CurrentTimeMillis();
            Log.Info("ProTempore", "Time: ", timeL);

            ContentValues eventValues = new ContentValues();
            eventValues.Put(CalendarContract.Events.InterfaceConsts.CalendarId, calID.ToString());
            eventValues.Put(CalendarContract.Events.InterfaceConsts.Title, (check + " at : " + "long: " + lon + ", lat: " + lat));
            eventValues.Put(CalendarContract.Events.InterfaceConsts.Description, "long: " + lon + ", lat: " + lat);
            eventValues.Put(CalendarContract.Events.InterfaceConsts.Dtstart, timeL);
            eventValues.Put(CalendarContract.Events.InterfaceConsts.Dtend, timeL);

            eventValues.Put(CalendarContract.Events.InterfaceConsts.EventTimezone, "GMT");
            eventValues.Put(CalendarContract.Events.InterfaceConsts.EventEndTimezone, "GMT");

            var uri = ContentResolver.Insert(CalendarContract.Events.ContentUri, eventValues);
            Log.Info("ProTempore", "Created an event in Calendar: ", calID);

        }


        private Location getLocation()
        {
            //Initialize the best known position.
            Location best = null;

            //For each provider in the provider list, try to fine tune the best position.
            foreach (String provider in providers)
            {
                //Ask for updated locations.
                Criteria locationCriteria = new Criteria() { Accuracy = Accuracy.NoRequirement, PowerRequirement = Power.NoRequirement };
                _locationManager.RequestSingleUpdate(locationCriteria, this, null);

                Location tempLoc = _locationManager.GetLastKnownLocation(provider);
                if (tempLoc == null)
                {
                    Log.Info("ProTempore", "No location using: {0}", provider);
                    continue;
                }
                if (best == null || (tempLoc.Accuracy < best.Accuracy))
                {
                    TimeSpan ts = (DateTime.UtcNow - new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc));
                    long timeCheck = long.Parse(Math.Floor(ts.TotalMilliseconds).ToString()) - tempLoc.Time;
                    Log.Info("ProTempore", "Now - LocationTime = {0}", timeCheck);
                    if (timeCheck < long.Parse("600000"))
                    {
                        best = tempLoc;
                        Log.Info("ProTempore", "Location refined using: {0}", provider);
                        Log.Info("ProTempore", "Accuracy: {0}", best.Accuracy);
                        Log.Info("ProTempore", "Latitude: {0}", best.Latitude.ToString());
                        Log.Info("ProTempore", "Longitude: {0}", best.Longitude.ToString());
                    }
                    else
                    {
                        Log.Info("ProTempore", "Timestamp of provider {0} is too old: {1} > 300000ms", provider, timeCheck.ToString());
                    }

                }

            }
            if (_liveLocation != null && (_liveLocation.Accuracy < best.Accuracy))
            {
                best = _liveLocation;
            }
            return best;
        }

        public void OnLocationChanged(Location location)
        {
            _liveLocation = location;
        }

        public void OnProviderDisabled(string provider)
        {
            throw new NotImplementedException();
        }

        public void OnProviderEnabled(string provider)
        {
            throw new NotImplementedException();
        }

        public void OnStatusChanged(string provider, [GeneratedEnum] Availability status, Bundle extras)
        {
            throw new NotImplementedException();
        }
    }

    
   }

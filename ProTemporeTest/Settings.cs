using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace ProTemporeTest
{
    [Activity(Label = "Settings")]
    public class Settings : Activity
    {

        Button back;

        string IP, PORT;
        string ipstore = "ipstore";
        string portstore = "portstore";

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            SetContentView(Resource.Layout.Settings);

            back = FindViewById<Button>(Resource.Id.Back_button);

            var prefs = Application.Context.GetSharedPreferences("ProTempore", FileCreationMode.Private);
            PORT = prefs.GetString(portstore, null);
            IP = prefs.GetString(ipstore, null);

            if (!(PORT == null || PORT == "")) {
                FindViewById<EditText>(Resource.Id.Port).Text = PORT;
                FindViewById<EditText>(Resource.Id.IP).Text = IP;
            }

            

            // Create your application here
            Button theButton = FindViewById<Button>(Resource.Id.Back_button);

            back.Click += delegate {
                BackClicked();
            };
        }

        private void BackClicked() {
            var prefs = Application.Context.GetSharedPreferences("ProTempore", FileCreationMode.Private);
            var prefEditor = prefs.Edit();
            PORT = FindViewById<EditText>(Resource.Id.Port).Text;
            IP = FindViewById<EditText>(Resource.Id.IP).Text;
            prefEditor.PutString(portstore, PORT);
            prefEditor.PutString(ipstore, IP);
            prefEditor.Commit();

            MainActivity.setAddress(IP, PORT);

            base.OnBackPressed();
        }
    }
}
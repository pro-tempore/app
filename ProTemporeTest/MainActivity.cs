﻿using System;
using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using Android.Media;
using Android.Util;
using SharedDepository;

/*
    This activity should be replaced with a splash screen and the authentication code. Basically this main activity is what is always started first, it runs 
    authentication and if success it swaps to the time stamp app (geoloc.cs)
*/

namespace ProTemporeTest
{
    [Activity(Label = "ProTemporeTest", Icon = "@drawable/icon")]
    public class MainActivity : Activity
    {

        //static string Username;
        //static string Password;
        TextView _info;
        static string username;
        static string token;
        static string serverAddr;

        static Menu menu;
        

        protected override void OnCreate(Bundle bundle)
        {
            //Handles state.
            base.OnCreate(bundle);

            //Set view to main layout
            SetContentView(Resource.Layout.Main);

            //Get the button.
            Button theButton = FindViewById<Button>(Resource.Id.android_button);
            Button settings = FindViewById<Button>(Resource.Id.settings_button);

            //Get the information text view.
            _info = FindViewById<TextView>(Resource.Id.Info_text);

            menu = new Menu(this, this.BaseContext);
            FindViewById<TextView>(Resource.Id.txtActionBarText).Text = "Login";

            // Get old username is such exists
            var prefs = Application.Context.GetSharedPreferences("ProTempore", FileCreationMode.Private);
            string usrname = prefs.GetString("username", null);
            if (usrname != null) {
                FindViewById<EditText>(Resource.Id.UserName).Text = usrname;
            }

            //get stored values for IP and Port
            prefs = Application.Context.GetSharedPreferences("ProTempore", FileCreationMode.Private);
            string PORT = prefs.GetString("portstore", null);
            string IP = prefs.GetString("ipstore", null);

            if (IP == null || PORT == null) {
                PORT = "30000";
                IP = "130.240.5.35";
            }

            serverAddr = "http://"+IP+":"+PORT+"/";

            //Catch the click event.
            theButton.Click += delegate {
                loginButtonPressed();
            };
            settings.Click += delegate {
                settingsClicked();
            };
        }

        private void settingsClicked() {
            Intent intent = new Intent(this.ApplicationContext, typeof(Settings));
            StartActivity(intent);
        }

        private async void loginButtonPressed()
        {
            username = FindViewById<EditText>(Resource.Id.UserName).Text;
            var password = FindViewById<EditText>(Resource.Id.Password).Text;

            if (username != null && password != null)
            {
                var loginHandler = new Login(username, password, serverAddr);
                try
                {
                    token = await loginHandler.login();
                }
                catch (Exception e)
                {
                    Log.Info("ProTempore", "Failed to log in: {0}", e.ToString());
                }
                if (token != "FAIL")
                {
                    // Store the succesfull username
                    var prefs = Application.Context.GetSharedPreferences("ProTempore", FileCreationMode.Private);
                    var prefEditor = prefs.Edit();
                    prefEditor.PutString("username", username);
                    prefEditor.Commit();

                    Intent intent = new Intent(this.ApplicationContext, typeof(GeoLoc));
                    StartActivity(intent);
                }
                else
                {
                    Log.Info("ProTempore", "Failed to login.");
                    _info.Text = "Failed to log in. Please check username and password.";
                }
            }
            else
            {
                _info.Text = "Please enter login info...";
            }
        }

        public static Menu getMenu() {
            return menu;
        }

        public static void setAddress(string IPa, string port) {
            serverAddr = "http://" + IPa + ":" + port + "/";
        }

        public static String getAddress()
        {
            Log.Info("ProTempore", serverAddr);
            return serverAddr;
        }

        public static String getToken()
        {
            Log.Info("ProTempore", token);
            return token;
        }

        public static String getUsername()
        {
            Log.Info("ProTempore", username);
            return username;
        }

        public static Menu GetMenu() {
            return menu;
        }
    }
}


﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;
using System.Net.Sockets;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Collections.Specialized;
using System.Threading.Tasks;
using System.Net.Http.Headers;

namespace SharedDepository
{
    public class SendStamp
    {
        string address;
        string time;

        private class Msg
        {
            public String operation { get; set; }
            public String email { get; set; }
            public String time { get; set; }
            public String longitude { get; set; }
            public String latitude { get; set; }
        }

        //constructor for a sendclass object.
        public SendStamp(String addressIn)
        {
            address = addressIn;
        }


        public async Task<Exception> transmit(String longitude, String latitude, String token, String username, String operation)

        {
            Console.WriteLine("I am sendstamp, token = {0}", token);
            //Get current time.
            time = DateTime.Now.ToString("yyyy'-'MM'-'dd HH':'mm':'ss");

            //Check that we have current time, current location and an address to post to.s
            if ((time ?? longitude ?? latitude ?? address) == null)
            {
                return new Exception("Required field is not set.");
            }
            Msg msg = new Msg() { operation = operation, time = time, longitude = longitude, latitude = latitude, email = username };
            string json = Newtonsoft.Json.JsonConvert.SerializeObject(msg);
            Console.WriteLine("json = {0}", json);
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(address);

                    client.Timeout = TimeSpan.FromSeconds(10);

                    client.DefaultRequestHeaders.Accept.Clear();

                    client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));

                    HttpContent content = new System.Net.Http.StringContent(json, System.Text.Encoding.UTF8, "application/json");

                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(token);//("token", token);//new System.Net.Http.Headers.AuthenticationHeaderValue(token); 

                    Console.WriteLine("Setting authorization token to: {0}", token);

                    var response = await client.PostAsync("api/v2/Timelogs/NewTimestamp", content);

                    var responseString = await response.Content.ReadAsStringAsync();

                    if (response.StatusCode.ToString() != "OK")
                    {
                        var errStr = "Server returned non-200 response: " + response.StatusCode.ToString();
                        throw new Exception(errStr);
                    }

                }
                return null;
            }
            catch (Exception e)
            {
                return e;
            }
           
        }

    }
}
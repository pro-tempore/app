﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;
using System.Net.Sockets;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Collections.Specialized;
using System.Threading.Tasks;

namespace SharedDepository
{
    public class Login
    {
        //Message sent to the login server.
        private class LogMsg
        {
            public String email { get; set; }
            public String password { get; set; }
        }

        //Token object returned by the login server.
        private class Tok
        {
            [JsonProperty("Token")]
            public String Token;
        }

        LogMsg logmsg;
        string loginServer;

        //constructor for a Login object
        public Login(String username, String password, String serverAddr)
        {
            logmsg = new LogMsg() { email = username, password = password };

            loginServer = serverAddr;
        }

        public async Task<string> login()
        {
            string token;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(loginServer);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                client.Timeout = TimeSpan.FromSeconds(10);
                string json = Newtonsoft.Json.JsonConvert.SerializeObject(logmsg);
                HttpContent content = new System.Net.Http.StringContent(json, System.Text.Encoding.UTF8, "application/json");
                System.Net.Http.HttpResponseMessage response;
                Console.WriteLine("Logging in as {0}, using password {1}", logmsg.email, logmsg.password);
                try
                {
                    response = await client.PostAsync("api/v1/login", content);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.ToString());
                    return "FAIL";
                }
                
                var responseString = await response.Content.ReadAsStringAsync();

                //Get the token from the response
                Tok t;
                try
                {
                    t = Newtonsoft.Json.JsonConvert.DeserializeObject<Tok>(responseString);
                }
                catch (Exception e)
                {
                    Console.WriteLine("Failed to deserialize answer from server: {0}", e.ToString());
                    return "FAIL";
                }
                

                if (responseString != "")
                {
                    token = t.Token;
                    return token;
                }
                else
                {
                    Console.WriteLine("Error, no token returned from server.");
                    return "FAIL";
                }
               
            }
        }
    }
}
 


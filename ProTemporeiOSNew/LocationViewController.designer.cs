// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace ProTemporeiOSNew
{
	[Register ("LocationViewController")]
	partial class LocationViewController
	{
		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UITextView infoTextView { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UISwitch inOrOutPicker { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UITextView latTextView { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UITextView longTextView { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UIButton transmitBtn { get; set; }

		void ReleaseDesignerOutlets ()
		{
			if (infoTextView != null) {
				infoTextView.Dispose ();
				infoTextView = null;
			}
			if (inOrOutPicker != null) {
				inOrOutPicker.Dispose ();
				inOrOutPicker = null;
			}
			if (latTextView != null) {
				latTextView.Dispose ();
				latTextView = null;
			}
			if (longTextView != null) {
				longTextView.Dispose ();
				longTextView = null;
			}
			if (transmitBtn != null) {
				transmitBtn.Dispose ();
				transmitBtn = null;
			}
		}
	}
}

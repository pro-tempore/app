﻿using System;
using System.Collections.Generic;
using System.Linq;
using EventKit;
using Foundation;
using UIKit;

namespace ProTemporeiOSNew
{
    public class Application
    {
        protected EKEventStore _eventStore;
        private static Application current;

        protected Application()
        {
            //Create the event store used by the calendar part of the application.
            _eventStore = new EKEventStore();
        }

        public EKEventStore EventStore
        {
            get { return _eventStore; }
        }

        public static Application Current
        {
            get { return current; }
        }
        static Application()
        {
            current = new Application();
        }

        // This is the main entry point of the application.
        static void Main(string[] args)
        {
            UIApplication.Main(args, null, "AppDelegate");
        }
    }
}
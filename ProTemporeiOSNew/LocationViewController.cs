
using System;
using System.Drawing;
using CoreLocation;
using Foundation;
using UIKit;
using SharedDepository;

namespace ProTemporeiOSNew
{
    public partial class LocationViewController : UIViewController
    {
        //Token is set when the view is segued into.
        public string token { get; set; }

        string _serverAddr;
        string _username;
        string _currentLongitude;
        string _currentLatitude;
        public static LocationManager _locationManager { get; set; }
        bool _hasLocation;

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            //Get the server address and username.
            _username = NSUserDefaults.StandardUserDefaults.StringForKey("PT_username");
            var nsIP = NSUserDefaults.StandardUserDefaults.StringForKey("PT_server_addr");
            var nsPort = NSUserDefaults.StandardUserDefaults.StringForKey("PT_server_port");
            _serverAddr = "http://" + nsIP + ":" + nsPort + "/";


            //Catch an updated location.
            _locationManager.LocationUpdated += HandleLocationChanged;

            Console.WriteLine("I am locationviewcontroller, token: {0}", token);

            //Handle clicks on the transmission button.
            transmitBtn.TouchUpInside += delegate
            {
                transmitButtonPressed();
            };
        }

        //Set up the location manager and make it start receiving updates.
        public LocationViewController(IntPtr handle) : base (handle)
        {
            _hasLocation = false;
            _locationManager = new LocationManager();
            _locationManager.StartLocationUpdates();
        }

        //HandleLocationChanged is run automatically when a new location is received by the iphone.
        private void HandleLocationChanged(object sender, LocationUpdatedEventArgs e)
        {
            //Get the location from the update event.
            CLLocation location = e.Location;

            //Extract the latitude and longitude.
            _currentLatitude = location.Coordinate.Latitude.ToString();
            _currentLongitude = location.Coordinate.Longitude.ToString();

            //Update the text views.
            longTextView.Text = "Longitude: " + _currentLongitude;
            latTextView.Text = "Latitude: " + _currentLatitude;

            //Declare that we now have a location.
            _hasLocation = true;

            Console.WriteLine("Location event handled.");
        }

        private async void transmitButtonPressed()
        {
            //Check that we have a location.
            if (_hasLocation)
            {
                Console.WriteLine("Transmit button pressed. Sending.");
                infoTextView.Text = "Transmitting location.";

                //Create a location transmission object.
                SendStamp stampSender = new SendStamp(_serverAddr);

                string operation;

                if (inOrOutPicker.On)
                {
                    operation = "Check out";
                    Console.WriteLine("Performing a check out!");
                }
                else
                {
                    operation = "Check in";
                    Console.WriteLine("Performing a check in!");
                }

                //Try to transmit.
                Exception err = await stampSender.transmit(_currentLongitude, _currentLatitude, token, _username, operation);
                if (err == null)
                {
                    infoTextView.Text = "Transmission successful";
                    Console.WriteLine("Tranmission successful");
                }
                else
                {
                    infoTextView.Text = "Failed to transmit.";
                    Console.WriteLine("Failed to transmit: {0}", err.ToString());
                }
            }
            else
            {
                Console.WriteLine("Transmit button pressed but no location available yet.");
                infoTextView.Text = "No location yet. Wait a while.";
            }
        }



    }
}
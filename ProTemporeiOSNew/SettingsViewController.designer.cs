// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace ProTemporeiOSNew
{
	[Register ("SettingsViewController")]
	partial class SettingsViewController
	{
		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UITextField ipTextField { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UITextField portTextField { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UIButton saveSettingsBtn { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UITextView settingsInfoTextView { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UIView SettingsView { get; set; }

		void ReleaseDesignerOutlets ()
		{
			if (ipTextField != null) {
				ipTextField.Dispose ();
				ipTextField = null;
			}
			if (portTextField != null) {
				portTextField.Dispose ();
				portTextField = null;
			}
			if (saveSettingsBtn != null) {
				saveSettingsBtn.Dispose ();
				saveSettingsBtn = null;
			}
			if (settingsInfoTextView != null) {
				settingsInfoTextView.Dispose ();
				settingsInfoTextView = null;
			}
			if (SettingsView != null) {
				SettingsView.Dispose ();
				SettingsView = null;
			}
		}
	}
}

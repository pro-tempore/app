﻿using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;
using SharedDepository;

namespace ProTemporeiOSNew
{
	partial class LoginViewController : UIViewController
	{
        public string token { get; set; }

        string _serverAddr;
        string _username;

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            //Set the background image.
            UIGraphics.BeginImageContext(this.View.Frame.Size);
            UIImage i = UIImage.FromFile("Images/main_background.png");
            i = i.Scale(this.View.Frame.Size);
            this.View.BackgroundColor = UIColor.FromPatternImage(i);

            //Set the color of the back button on the navigation controller. I found no way to do this in the storyboard.
            this.NavigationController.NavigationBar.TintColor = new UIColor(47f/255f, 87f/255f, 93f/255f, 1);

            //Set the previously used username (if it exists).
            var nsUsername = NSUserDefaults.StandardUserDefaults.StringForKey("PT_username");
            if (nsUsername != null)
            {
                usernameField.Text = nsUsername;
            }


            submitBtn.TouchUpInside += delegate
            {
                Console.WriteLine("Logging in and starting time stamping application.");
                loginButtonPressed();
            };

        }

        private async void loginButtonPressed()
        {
            //Set the username variable that will be passed to the rest of the application.
            _username = usernameField.Text;

            //Construct the server address with the values stored in the user defaults.
            var nsIP = NSUserDefaults.StandardUserDefaults.StringForKey("PT_server_addr");
            var nsPort = NSUserDefaults.StandardUserDefaults.StringForKey("PT_server_port");
            _serverAddr = "http://" + nsIP + ":" + nsPort + "/";
            Console.WriteLine("Trying to log in to: {0}", _serverAddr);

            //Make sure port and ip are set.
            if ((nsIP ?? nsPort) == null)
            {
                infoTextView.Text = "Port or IP not set.";
                Console.WriteLine("Failed to log in. Port or IP not set");
                return;
            }
            Console.WriteLine("Logging in to {0}", _serverAddr);
            //Create a login handler with the supplied credentials.
            var loginHandler = new Login(usernameField.Text, passwordField.Text, _serverAddr);

            //Get the token from a login attempt.
            token = await loginHandler.login();

            //Unless we have failed then segue to the menu.
            if (token != "FAIL")
            {
                infoTextView.Text = "Successfully logged in";

                //Save the username.
                NSUserDefaults.StandardUserDefaults.SetString(_username, "PT_username");

                //Segue to the menu.
                PerformSegue("gotoMenuSegue", this);

            }
            else
            {
                infoTextView.Text = "Failed to log in.";
                Console.WriteLine("Failed to log in.");
            }
        }

        public LoginViewController (IntPtr handle) : base (handle)
        {
        }

        //PrepareForSegue is run automatically when a segue is performed.
        //We use it to pass the needed variables to the rest of 
        public override void PrepareForSegue(UIStoryboardSegue segue, NSObject sender)
        {
            base.PrepareForSegue(segue, sender);

            // Check that the correct segue is triggered. Used to cast correctly.
            if (segue.Identifier.Equals("gotoMenuSegue"))
            {
                Console.WriteLine("Preparing segue with token: {0}", token);
                var targetController = segue.DestinationViewController as MenuViewController;
                targetController.token = token; 
            }
        }
    }
}


using System;
using System.Drawing;

using Foundation;
using UIKit;
using EventKit;

namespace ProTemporeiOSNew
{
    public partial class MenuViewController : UIViewController
    {
        //Token is set when the view is segued into.
        public string token { get; set; }

        static bool UserInterfaceIdiomIsPhone
        {
            get { return UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Phone; }
        }

        public MenuViewController(IntPtr handle) : base(handle)
        {
        }

        public override void DidReceiveMemoryWarning()
        {
            // Releases the view if it doesn't have a superview.
            base.DidReceiveMemoryWarning();

            // Release any cached data, images, etc that aren't in use.
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            Console.WriteLine("I am the menu, I reveived this token: {0}", token);

            //Special case for the schedule button since it launches the phones default calendar.
            gotoSchedule.TouchUpInside += delegate
            {
                //Request access to the event store.
                
                Application.Current.EventStore.RequestAccess(EKEntityType.Event, (bool granted, NSError e) => {
                    if (!granted)
                    {
                        Console.WriteLine("User did not grant access to the calendar");
                        return;
                    }
                });

                var calUrl = new NSUrl("calshow://");
                if (UIApplication.SharedApplication.CanOpenUrl(calUrl))
                {
                    try
                    {
                        UIApplication.SharedApplication.OpenUrl(calUrl);
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("Error when opening calendar: {0}", e.ToString());
                    }
                }
                else
                {
                    Console.WriteLine("Failed to open calendar.");
                }
                
          
            };
           
        }

        //PrepareForSegue is run automatically when a segue is performed.
        //We use it to pass relevant variables to the next view.
        public override void PrepareForSegue(UIStoryboardSegue segue, NSObject sender)
        {
            base.PrepareForSegue(segue, sender);
            Console.WriteLine("Preparing segue with token: {0}", token);
            // Check which segue is triggered so we can cast correctly.
            if (segue.Identifier.Equals("gotoTimeStampSegue"))
            {
                var targetController = segue.DestinationViewController as LocationViewController;
                targetController.token = token;
            }
        }

    }
}
﻿using System;
using System.Collections.Generic;
using System.Text;
using CoreLocation;
using UIKit;

namespace ProTemporeiOSNew
{ 
 public class LocationManager
{
    protected CLLocationManager locMgr;

    public LocationManager()
    {
        //Create a manager and ask for permission to use the location manager.
        this.locMgr = new CLLocationManager();
        if (UIDevice.CurrentDevice.CheckSystemVersion(8, 0))
        {
            locMgr.RequestAlwaysAuthorization(); 
        }
    }

    public CLLocationManager LocMgr
    {
        get { return this.locMgr; }
    }

    public void StartLocationUpdates()
    {
        //Check if we have access to the location services.
        if (CLLocationManager.LocationServicesEnabled)
        {
            //set the desired accuracy, in meters
            LocMgr.DesiredAccuracy = 1;
            LocMgr.LocationsUpdated += (object sender, CLLocationsUpdatedEventArgs e) =>
            {
                // fire our custom Location Updated event
                LocationUpdated(this, new LocationUpdatedEventArgs(e.Locations[e.Locations.Length - 1]));
            };
            LocMgr.StartUpdatingLocation();
        }
    }

    public event EventHandler<LocationUpdatedEventArgs> LocationUpdated = delegate { };
}
}


using System;
using System.Drawing;

using Foundation;
using UIKit;

namespace ProTemporeiOSNew
{
    public partial class SettingsViewController : UIViewController
    {
        public override void ViewDidLoad()
        {
            base.ViewDidLoad();


            //Get the current values (if they exist).
            var nsIP = NSUserDefaults.StandardUserDefaults.StringForKey("PT_server_addr");
            var nsPort = NSUserDefaults.StandardUserDefaults.StringForKey("PT_server_port");

            //If they don't exist, set dummy data so the user knows the preferred format.
            if (nsIP == null)
            {
                nsIP = "1.2.3.4";
            }
            if (nsPort == null)
            {
                nsPort = "12345";
            }

            //Display the current values or dummy data.
            ipTextField.Placeholder = nsIP;
            portTextField.Placeholder = nsPort;

            saveSettingsBtn.TouchUpInside += delegate
            {
                saveSettingsButtonPressed(ipTextField.Text, portTextField.Text);
            };

        }
        public SettingsViewController(IntPtr handle) : base(handle)
        {

        }

        private void saveSettingsButtonPressed(string serverAddr, string serverPort)
        {
            if (serverAddr != "")
            {
                NSUserDefaults.StandardUserDefaults.SetString(serverAddr, "PT_server_addr");
                Console.WriteLine("Server address updated");
            }
            if (serverPort != "")
            {
                NSUserDefaults.StandardUserDefaults.SetString(serverPort, "PT_server_port");
                Console.WriteLine("Server port updated");
            }

            settingsInfoTextView.Text = "Settings updated.";
            
            

        }
    }
}
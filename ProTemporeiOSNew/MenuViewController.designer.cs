// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace ProTemporeiOSNew
{
	[Register ("MenuViewController")]
	partial class MenuViewController
	{
		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UIButton gotoSchedule { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UIButton gotoSettings { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UIButton gotoTimeStamp { get; set; }

		void ReleaseDesignerOutlets ()
		{
			if (gotoSchedule != null) {
				gotoSchedule.Dispose ();
				gotoSchedule = null;
			}
			if (gotoSettings != null) {
				gotoSettings.Dispose ();
				gotoSettings = null;
			}
			if (gotoTimeStamp != null) {
				gotoTimeStamp.Dispose ();
				gotoTimeStamp = null;
			}
		}
	}
}

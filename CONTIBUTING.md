# PRO-TEMPORE
## Dev instructions

The dev model for this project follows the [common git branching model](http://nvie.com/posts/a-successful-git-branching-model/). Please read this before contributing.

The project contains these main branches:  
* The `master` branch is for *production-ready code*. It is for releases only.
* The `develop` branch reflects the latest changes staged for release. When `develop` reaches a stable, release-ready state, it is merged into `master`.

There are also these temporary branches:  
* `hotfix` branches are for when there is an issue with `master`. Hotfixes are merged into `master` and `develop` when ready.
* `release` branches are staging areas for the next stable release. They are branched off from `develop` and merged back into `develop` and `master`.
* `feature` branches are for developing specific features. They branch from and merge back into ´develop`.